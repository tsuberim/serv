import * as yup from 'yup';

const x: number = false;

export class IntDef { type = 'int'; public static singelton = new IntDef(); private constructor() { } }
export class FloatDef { type = 'float'; public static singelton = new FloatDef(); private constructor() { } }
export class BooleanDef { type = 'boolean'; public static singelton = new BooleanDef(); private constructor() { } }
export class StringDef { type = 'string'; public static singelton = new StringDef(); private constructor() { } }

export type ScalarDef = IntDef | FloatDef | BooleanDef | StringDef;
export function isScalarDef(x: any): x is ScalarDef {
  return x instanceof IntDef || x instanceof FloatDef || x instanceof BooleanDef || x instanceof StringDef;
}

export class RequiredDef {
  constructor(public ofType: TypeDef) {
    if (!isTypeDef(ofType)) {
      throw new Error(`Expected a type definition, got ${ofType}`)
    }
  }
}

export class ListDef {
  constructor(public ofType: TypeDef) {
    if (!isTypeDef(ofType)) {
      throw new Error(`Expected a type definition, got ${ofType}`)
    }
  }
}

export class ObjectDef {
  constructor(public typeMap: { [key: string]: TypeDef }) {
    if (typeof typeMap !== 'object') {
      throw new Error(`Expected a map of types, got ${typeMap}`)
    }
    for (const [key, value] of Object.entries(typeMap)) {
      if (!isTypeDef(value)) {
        throw new Error(`Expected a type definition, got ${value} for key '${key}'`)
      }
    }
  }
}

export type TypeDef = ScalarDef | RequiredDef | ListDef | ObjectDef
export function isTypeDef(x: any): x is TypeDef {
  return isScalarDef(x) || x instanceof RequiredDef || x instanceof ListDef || x instanceof ObjectDef
}


export function nullable(type: TypeDef): TypeDef {
  while (type instanceof RequiredTypeDef) {
    type = type.ofType;
  }
  return type;
}

export interface Walker<T> {
  int: T,
  float: T,
  boolean: T,
  string: T,
  required: (t: T) => T,
  list: (t: T) => T,
  object: (t: { [key: string]: T }) => T,
}

export function walk<T>(type: TypeDef, walker: Walker<T>): T {
  if (isScalarDef(type)) {
    if (type instanceof IntDef) {
      return walker.int
    } else if (type instanceof FloatDef) {
      return walker.float
    } else if (type instanceof BooleanDef) {
      return walker.boolean
    } else if (type instanceof StringDef) {
      return walker.string
    } else {
      throw new Error(`Impossible`)
    }
  } else if (type instanceof RequiredDef) {
    return walker.required(walk(type.ofType, walker))
  } else if (type instanceof ListDef) {
    return walker.list(walk(type.ofType, walker))
  } else if (type instanceof ObjectDef) {
    return walker.object(
      Object.entries(type.typeMap)
        .reduce((acc, [key, value]) => ({
          ...acc,
          [key]: walk(value, walker)
        }), {})
    )
  } else {
    throw new Error(`Impossible`)
  }
}

export function toYup(type: TypeDef): yup.MixedSchema<any> {
  return walk<yup.MixedSchema<any>>(type, {
    int: yup.number().integer(),
    float: yup.number(),
    boolean: yup.boolean(),
    string: yup.string(),
    required: schema => schema.required(),
    list: schema => yup.array(schema),
    object: map => yup.object().shape(map)
  })
}