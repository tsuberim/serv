#!/usr/bin/env node

import * as yargs from 'yargs';
import * as ora from 'ora';
import * as express from 'express';
import * as path from 'path';
import * as bodyParser from 'body-parser';
import * as http from 'http';
import * as socketIO from 'socket.io'
import * as cors from 'cors'
import { NodeVM } from 'vm2';
import * as ts from 'typescript'
import * as fs from 'fs';
import * as yup from 'yup';


export interface Options {
  port: number;
  allowedOrigins: string;
  frontend?: string;
  config?: string;
}

export const defaultOptions: Options = {
  allowedOrigins: '*',
  port: 1234
}

export async function run(opts: Partial<Options>) {
  const options: Options = { ...defaultOptions, ...opts }
  const { port, allowedOrigins, frontend, config } = options;

  const logger = ora();
  const app = express();

  app.use(bodyParser.json());
  logger.info(`Allowed origins '${allowedOrigins}'`)
  app.use(cors({ origin: allowedOrigins }))

  if (frontend) {
    logger.info(`Serving a single-page-app from path '${frontend}'`)
    const dir = path.resolve(frontend);
    app.use(express.static(dir));
    app.get('/', (req, res) => res.sendFile(path.join(dir, 'index.html')));
  }

  const server = new http.Server(app);
  const io = socketIO(server);

  server.listen(port, () => logger.info(`Server listening on port ${port}`))
}

export async function runCLI() {
  return run(yargs.option('port', {
    alias: 'p',
    type: 'number',
    desc: 'The port number to listen on',
    default: defaultOptions.port
  }).option('frontend', {
    alias: 'f',
    type: 'string',
    desc: 'An optional path to a frontend directory to serve static files from as a Single-Page-App',
    default: defaultOptions.frontend
  }).option('allowed-origins', {
    alias: 'o',
    type: 'string',
    default: defaultOptions.allowedOrigins
  }).option('config', {
    alias: 'c',
    type: 'string',
    desc: 'An optional config javascript or typescript module',
    default: defaultOptions.config
  }).argv)
}

if (require.main === module) {
  runCLI()
}